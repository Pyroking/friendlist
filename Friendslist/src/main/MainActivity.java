package main;

import helper.BuildDBHelper;
import helper.DateFormatHelper;
import helper.UIHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.friendslist.R;

import model.Friend;

import adapter.DBAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v4.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class MainActivity extends Activity {
	BuildDBHelper buildDBHelper;
	public UIHelper uiHelper;

	public DBAdapter db;
	public DateFormatHelper dateFormat;

	public SimpleAdapter simpleAdpt = null;
	public ListView lv = null;
	public List<Friend> friendList;
	public List<Map<String, String>> contactList = new ArrayList<Map<String, String>>();

	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;

	public boolean checkbox_with_year;
	public boolean checkbox_without_year;
	public boolean checkbox_name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		loadPref();

		uiHelper = new UIHelper(this);
		buildDBHelper = new BuildDBHelper();
		dateFormat = new DateFormatHelper();

		db = new DBAdapter(this);

		friendList = db.getAllContacts();

		mTitle = mDrawerTitle = getTitle();

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, uiHelper.mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				uiHelper.removeSoftKeyboard();
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		uiHelper.mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			uiHelper.displayView(0, 0);
			
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action bar actions click
		switch (item.getItemId()) {
		case R.id.action_settings:
			uiHelper.displayView(3, 0);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = uiHelper.mDrawerLayout
				.isDrawerOpen(uiHelper.mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	private void loadPref() {
		SharedPreferences mySharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);

		checkbox_with_year = mySharedPreferences.getBoolean(
				"checkbox_name_birthday_with_year", false);
		checkbox_without_year = mySharedPreferences.getBoolean(
				"checkbox_name_birthday_without_year", false);
		checkbox_name = mySharedPreferences.getBoolean("checkbox_name", false);
	}

	public void initList() {
		loadPref();
		/*
		 * simpleAdpt = new SimpleAdapter(this, contactList,
		 * android.R.layout.simple_list_item_1, new String[] { "contact" }, new
		 * int[] { android.R.id.text1 }); lv.setAdapter(simpleAdpt);
		 * 
		 * friendList.clear(); friendList = db.getAllContacts();
		 * contactList.clear();
		 * 
		 * if (checkbox_with_year) { Collections.sort(friendList,
		 * Friend.BIRTHDAY_COMPARATOR); for (Friend cn : friendList) {
		 * contactList.add(buildDBHelper.createContactList("contact",
		 * cn.toStringNameBirthWithYear())); } }
		 * 
		 * if (checkbox_without_year) { Collections.sort(friendList,
		 * Friend.BIRTHDAY_COMPARATOR_EXCLUDING_YEAR); for (Friend cn :
		 * friendList) {
		 * contactList.add(buildDBHelper.createContactList("contact",
		 * cn.toStringNameBirthWithOutYear())); } }
		 * 
		 * if (checkbox_name) { Collections.sort(friendList,
		 * Friend.NAME_COMPARATOR); for (Friend cn : friendList) {
		 * contactList.add(buildDBHelper.createContactList("contact",
		 * cn.toStringName())); } }
		 */
		new initListAsyncTask(MainActivity.this).execute();
	}

	private class initListAsyncTask extends
			AsyncTask<String, Void, ArrayList<HashMap<String, String>>> {

		private ProgressDialog dialog;
		private Context context;
		List<Map<String, String>> contactList = MainActivity.this.contactList;

		public initListAsyncTask(MainActivity activity) {
			context = activity;
			dialog = new ProgressDialog(context);
		}

		protected void onPreExecute() {
			this.dialog.setMessage(getResources().getString(R.string.loading_friends));
			this.dialog.show();
		}

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(
				String... params) {
			friendList.clear();
			friendList = db.getAllContacts();
			contactList.clear();

			if (checkbox_with_year) {
				Collections.sort(friendList, Friend.BIRTHDAY_COMPARATOR);
				for (Friend cn : friendList) {
					contactList.add(buildDBHelper.createContactList("contact",
							cn.toStringNameBirthWithYear()));
				}
			}

			if (checkbox_without_year) {
				Collections.sort(friendList,
						Friend.BIRTHDAY_COMPARATOR_EXCLUDING_YEAR);
				for (Friend cn : friendList) {
					contactList.add(buildDBHelper.createContactList("contact",
							cn.toStringNameBirthWithOutYear()));
				}
			}

			if (checkbox_name) {
				Collections.sort(friendList, Friend.NAME_COMPARATOR);
				for (Friend cn : friendList) {
					contactList.add(buildDBHelper.createContactList("contact",
							cn.toStringName()));
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> result) {
			simpleAdpt = new SimpleAdapter(MainActivity.this, contactList,
					android.R.layout.simple_list_item_1,
					new String[] { "contact" },
					new int[] { android.R.id.text1 });
			lv.setAdapter(simpleAdpt);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
		}
	}
}
