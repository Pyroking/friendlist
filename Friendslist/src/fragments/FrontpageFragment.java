package fragments;

import java.util.HashMap;
import java.util.List;

import main.MainActivity;
import model.Friend;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.friendslist.R;

public class FrontpageFragment extends Fragment {

	protected static final String TAG = MainActivity.class.getSimpleName();

	// Search EditText
	EditText inputSearch;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.fragment_frontpage,
				container, false);
		((MainActivity) getActivity()).lv = (ListView) rootView
				.findViewById(R.id.listView);
		((MainActivity) getActivity()).setTitle("Home");
		
		//Catch portrait exception 
		try {
		((MainActivity) getActivity()).initList();
		} catch(NullPointerException ex) {
		}

		// React to user clicks on item
		((MainActivity) getActivity()).lv
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					public void onItemClick(AdapterView<?> parentAdapter,
							View view, int position, long id) {

						// We know the View is a TextView so we can cast it
						TextView clickedView = (TextView) view;
						String selected = clickedView.getText().toString();

						String result = getItemId(selected);
						getToFriendFragment(Integer.valueOf(result));
					}
				});

		registerForContextMenu(((MainActivity) getActivity()).lv);

		inputSearch = (EditText) rootView.findViewById(R.id.inputSearch);
		inputSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				//Catch portrait exception
				try {
				// When user changed the Text
				((MainActivity) getActivity()).simpleAdpt.getFilter()
						.filter(cs);
				} catch(NullPointerException ex) {
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {
			}
		});

		return rootView;
	}

	// We want to create a context Menu when the user long click on an item
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {

		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterContextMenuInfo aInfo = (AdapterContextMenuInfo) menuInfo;

		// We know that each row in the adapter is a Map
		HashMap<?, ?> map = (HashMap<?, ?>) ((MainActivity) getActivity()).simpleAdpt
				.getItem(aInfo.position);

		menu.setHeaderTitle("" + map.get(R.string.contact));
		menu.add(1, 1, 1, R.string.details);
		menu.add(1, 2, 2, R.string.delete);
	}

	private void delete(final int rowId) {
		new AlertDialog.Builder(getActivity())
				.setTitle(R.string.are_you_sure)
				.setPositiveButton(R.string.delete,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								((MainActivity) getActivity()).db
										.deleteContact(rowId);

								((MainActivity) getActivity()).initList();
								((MainActivity) getActivity()).simpleAdpt
										.notifyDataSetChanged();
								// lv.invalidateViews();
								((MainActivity) getActivity()).lv
										.refreshDrawableState();

							}
						})
				.setNegativeButton(R.string.cancel,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// ignore, just dismiss
							}
						}).show();
	}

	// This method is called when user selects an Item in the Context menu
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		int itemId = item.getItemId();

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();
		String selected = ((TextView) info.targetView).getText().toString();
		// String selected = info.toString();

		String result = getItemId(selected);
		
		if (itemId == 1 && result != null) {
			getToFriendFragment(Integer.valueOf(result));
		}

		if (itemId == 2 && result != null) {
			delete(Integer.valueOf(result));
		}
		return true;
	}
	
	public String getItemId(String selected) {
		List<Friend> friendList = ((MainActivity) getActivity()).db.getAllContacts();
		String result = null;
		
		for (Friend friendLoop : friendList) {	
			String preResult = null;
			if (((MainActivity) getActivity()).checkbox_with_year) {
			preResult = (friendLoop.toString());
			} else if (((MainActivity) getActivity()).checkbox_without_year) {
			preResult = (friendLoop.toStringNameBirthWithOutYear());
			} else if (((MainActivity) getActivity()).checkbox_name) {
			preResult = (friendLoop.toStringName());
		    }
			if (preResult.equals(selected)) {
				result = friendLoop.getId();
			}
		}
		return result;
	}

	private void getToFriendFragment(long id) {
	   ((MainActivity) getActivity()).uiHelper.displayView(2, id);
	}
}
