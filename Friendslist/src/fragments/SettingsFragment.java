package fragments;

import main.MainActivity;

import com.example.friendslist.R;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

public class SettingsFragment extends PreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);

		// update selected item and title, then close the drawer
		((MainActivity) getActivity()).setTitle(R.string.action_settings);
//		try {
//		((MainActivity) getActivity()).initList();
//		} catch(NullPointerException ex) {
//		}
		final CheckBoxPreference p3 = (CheckBoxPreference) findPreference("checkbox_name_birthday_with_year");
		final CheckBoxPreference p = (CheckBoxPreference) findPreference("checkbox_name_birthday_without_year");
		final CheckBoxPreference p2 = (CheckBoxPreference) findPreference("checkbox_name");
		
		setChecked(p, p2, p3);
		SharedPreferences.OnSharedPreferenceChangeListener changeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
			public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
				if (key.equals("checkbox_name_birthday_with_year")) {
					// Reset other items
					p3.setEnabled(true);
					p.setChecked(false);
					p2.setChecked(false);
					setChecked(p, p2, p3);
				}
				if (key.equals("checkbox_name_birthday_without_year")) {
					// Reset other items
					p.setEnabled(true);
					p3.setChecked(false);
					p2.setChecked(false);
					setChecked(p, p2, p3);
				}
				if (key.equals("checkbox_name")) {
					p2.setEnabled(true);
					p.setChecked(false);
					p3.setChecked(false);
					setChecked(p, p2, p3);
				}
			}
		};
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		prefs.registerOnSharedPreferenceChangeListener(changeListener);
	}
	
	public void setChecked(CheckBoxPreference p, CheckBoxPreference p2, CheckBoxPreference p3) {
		if (p3.isChecked()) {
			p3.setEnabled(false);
		} else if (p.isChecked()) {
			p.setEnabled(false);
		} else if (p2.isChecked()) {
			p2.setEnabled(false);
		}
	}
}
