package fragments;

import java.util.Calendar;
import java.util.List;

import main.MainActivity;
import model.Friend;

import com.example.friendslist.R;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

public class FriendFragment extends Fragment implements OnClickListener {

	Calendar myCalendar = Calendar.getInstance();
	protected static final String TAG = MainActivity.class.getSimpleName();

	static final int DATE_DIALOG_ID = 999;

	EditText nameValueInput;
	EditText emailValueInput;
	EditText birthdayValueInput;

	String fragmentType;
	String IdString;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		Bundle bundle = this.getArguments();
		fragmentType = bundle.getString("type");
		long Id = bundle.getLong("position", 0);

		View rootView = inflater.inflate(R.layout.fragment_friend, container,
				false);

		Button b = (Button) rootView.findViewById(R.id.buttonSaveFriend);
		b.setOnClickListener(this);

		nameValueInput = (EditText) rootView.findViewById(R.id.editTextName);
		emailValueInput = (EditText) rootView.findViewById(R.id.editTextEmail);
		birthdayValueInput = (EditText) rootView
				.findViewById(R.id.editTextBirthday);

		birthdayValueInput.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new DatePickerDialog(getActivity(), date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

		if (Long.valueOf(Id) != null && fragmentType != "add") {
			// new getContactAsyncTask(((MainActivity)
			// getActivity())).execute(Id);
			Friend friend = ((MainActivity) getActivity()).db.getContact(Id);
			if (friend != null) {
				nameValueInput.setText(friend.getName());
				emailValueInput.setText(friend.getEmail());
				birthdayValueInput
						.setText(((MainActivity) getActivity()).dateFormat
								.getDateTime(friend.getBirthday()));
				IdString = friend.getId();
			}
		}

		return rootView;
	}

	@Override
	public void onClick(View v) {

		String name = nameValueInput.getText().toString();
		if (name.trim().equals("")) {
			nameValueInput.setError(getResources().getString(
					R.string.name_error));
		}

		String email = emailValueInput.getText().toString();
		if (email.trim().equals("")) {
			emailValueInput.setError(getResources().getString(
					R.string.email_error));
		}

		String birthday = birthdayValueInput.getText().toString();
		if (birthday.trim().equals("")) {
			birthdayValueInput.setError(getResources().getString(
					R.string.birthday_error));
		}

		if (!birthday.isEmpty() && !email.isEmpty() && !name.isEmpty()) {
			Friend friend = new Friend(name, email,
					((MainActivity) getActivity()).dateFormat
							.passStringToDate(birthday));

			List<Friend> friendList = ((MainActivity) getActivity()).db
					.getAllContacts();

			if (fragmentType == "add") {
				boolean stopLoop = false;
				for (Friend cn : friendList) {
					if (cn.equals(friend)) {
						((MainActivity) getActivity()).uiHelper.alertDialog();
						stopLoop = true;
						break;
					}
				}
				if (!stopLoop) {
					((MainActivity) getActivity()).db.addContact(friend);

					Toast.makeText(getActivity(), R.string.add_to_friendlist,
							Toast.LENGTH_SHORT).show();
					((MainActivity) getActivity()).uiHelper.displayView(0, 0);
				}
			} else {
				friend.setId(IdString);
				((MainActivity) getActivity()).db.updateContact(friend);
				((MainActivity) getActivity()).initList();

				Toast.makeText(getActivity(), R.string.edit_friend,
						Toast.LENGTH_SHORT).show();
				((MainActivity) getActivity()).uiHelper.displayView(0, 0);
			}
		}
	}

	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateLabel();
		}
	};

	private void updateLabel() {
		birthdayValueInput.setText(((MainActivity) getActivity()).dateFormat
				.getDateTime(myCalendar.getTime()));
	}

	// private class getContactAsyncTask extends AsyncTask<Long, Void, Friend> {
	// private ProgressDialog dialog;
	// private MainActivity activity;
	// private Context context;
	//
	// public getContactAsyncTask(MainActivity activity) {
	// this.activity = activity;
	// context = activity;
	// dialog = new ProgressDialog(context);
	// }
	//
	// @Override
	// protected Friend doInBackground(Long... Id) {
	// Friend friend = ((MainActivity) getActivity()).db.getContact(Id[0]);
	// return friend;
	// }
	//
	// protected void onPreExecute() {
	// this.dialog.setMessage("Loading friend...");
	// this.dialog.show();
	// }
	//
	// protected void onPostExecute(Friend friend) {
	// if (friend != null) {
	// nameValueInput.setText(friend.getName());
	// emailValueInput.setText(friend.getEmail());
	// birthdayValueInput.setText(((MainActivity) getActivity()).dateFormat
	// .getDateTime(friend.getBirthday()));
	// IdString = friend.getId();
	// }
	// if (dialog.isShowing()) {
	// dialog.dismiss();
	// }
	// }
	// }
}
