package adapter;

import helper.DateFormatHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Friend;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter extends SQLiteOpenHelper {
	static final String KEY_ID = "_id";
	static final String KEY_NAME = "name";
	static final String KEY_EMAIL = "email";
	static final String KEY_BIRTHDAY = "birthday";
	static final String TAG = "DBAdapter";
	static final String DATABASE_NAME = "friendslist";
	static final String DATABASE_TABLE = "contacts";
	static final int DATABASE_VERSION = 5;
	DateFormatHelper format = new DateFormatHelper();
	Date date = null;

	private static final String DATABASE_CREATE = "create table "
			+ DATABASE_TABLE + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_NAME
			+ " text not null, " + KEY_EMAIL + " text not null, "
			+ KEY_BIRTHDAY + " DATETIME not null" + ");";

	SQLiteDatabase db;

	public DBAdapter(Context ctx) {
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.execSQL(DATABASE_CREATE);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS contacts");
		onCreate(db);
	}

	// ---deletes a particular contact---
	public void deleteContact(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(DATABASE_TABLE, KEY_ID + " = ?",
				new String[] { String.valueOf(id) });
		db.close();
	}

	// ---retrieves all the contacts---
	public List<Friend> getAllContacts() {
		List<Friend> friendList = new ArrayList<Friend>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + DATABASE_TABLE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Friend friend = new Friend();
				friend.setId(cursor.getString(0));
				friend.setName(cursor.getString(1));
				friend.setEmail(cursor.getString(2));
				friend.setBirthday(format.passStringToDate(cursor.getString(3)));
				// Adding contact to list
				friendList.add(friend);
			} while (cursor.moveToNext());
		}

		// return contact list
		return friendList;
	}

	// ---retrieves a particular contact---
	public Friend getContact(long id) {

		Friend friend= null;
		
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DATABASE_TABLE + " WHERE " + KEY_ID + "=" + id;
        Cursor c = db.rawQuery(selectQuery, null);
        
		if (c.moveToFirst()) {
			do {
				if (Integer.parseInt(c.getString(0)) == id) {
				friend = new Friend();
				friend.setId(c.getString(0));
				friend.setName(c.getString(1));
				friend.setEmail(c.getString(2));
				friend.setBirthday(format.passStringToDate(c.getString(3)));
				// Adding contact to list
				}
			} while (c.moveToNext());
		}
            
        return friend;
	}

	// ---updates a contact---
	public int updateContact(Friend friend) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, friend.getName());
		values.put(KEY_EMAIL, friend.getEmail());
		values.put(KEY_BIRTHDAY, format.getDateTime(friend.getBirthday()));

		// updating row
		return db.update(DATABASE_TABLE, values, KEY_ID + " = ?",
				new String[] { String.valueOf(friend.getId()) });
	}

	// ---delete all contacts----
	public void removeAll() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
		onCreate(db);
	}

	public void addContact(Friend friend) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, friend.getName());
		values.put(KEY_EMAIL, friend.getEmail());
		values.put(KEY_BIRTHDAY, format.getDateTime(friend.getBirthday()));
		

		// Inserting Row
		db.insert(DATABASE_TABLE, null, values);
		db.close(); // Closing database connection
	}
}
