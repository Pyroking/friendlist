package helper;

import java.util.ArrayList;

import main.MainActivity;
import model.NavDrawerItem;

import com.example.friendslist.R;

import fragments.FriendFragment;
import fragments.FrontpageFragment;
import fragments.SettingsFragment;

import adapter.NavDrawerListAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;

public class UIHelper {

	public String[] navMenuTitles;
	MainActivity mainActivity;
	public DrawerLayout mDrawerLayout;
	public ListView mDrawerList;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	AlertDialog.Builder alertDialogBuilder;
	
	// slide menu items
	private TypedArray navMenuIcons;

	public UIHelper(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
		
		alertDialogBuilder = new AlertDialog.Builder(mainActivity);
		
		mDrawerLayout = (DrawerLayout) mainActivity
				.findViewById(R.id.drawer_layout);
		
		navMenuTitles = mainActivity.getResources().getStringArray(
				R.array.nav_drawer_items);
		
		mDrawerList = (ListView) mainActivity.findViewById(R.id.list_slidermenu);
		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
		
		navMenuIcons = mainActivity.getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);
		
		navDrawerItems = new ArrayList<NavDrawerItem>();
		// adding nav drawer items to array
		// Home
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0],
				navMenuIcons.getResourceId(0, -1)));

		// Add Friend
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1],
				navMenuIcons.getResourceId(1, -1)));
		
		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(mainActivity.getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);
		
		// Recycle the typed array
		navMenuIcons.recycle();
	}

	public void displayView(int position, long id) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new FrontpageFragment();
			break;
		case 1:
			fragment = new FriendFragment();
			break;
		case 2:
			fragment = new FriendFragment();
			break;
		case 3:
			fragment = new SettingsFragment();
			break;

		default:
			break;
		}

		if (fragment != null) {
			Bundle bundle = new Bundle();
			
			if (position == 1) {
				bundle.putString("type", "add");
				fragment.setArguments(bundle);
			}
			
			if (position == 2) {
				bundle.putString("type", "edit");
				bundle.putLong("position", id);
				fragment.setArguments(bundle);
				mainActivity.setTitle(R.string.edit_friend);
			}
			
			if (position == 3) {
				mainActivity.setTitle(R.string.action_settings);
			}
			
			FragmentManager fragmentManager = mainActivity.getFragmentManager();
			FragmentTransaction fragmentTransaction = fragmentManager
					.beginTransaction();
			
			fragmentTransaction.replace(R.id.frame_container, fragment);
			fragmentTransaction.addToBackStack(null);
			//fragmentTransaction.disallowAddToBackStack();
			// Commit the transaction
			fragmentTransaction.commit();
			
//			FragmentManager fragmentManager = mainActivity.getFragmentManager();
//			fragmentManager.beginTransaction()
//					.replace(R.id.frame_container, fragment).commit();

			if (position == 0 || position == 1) {
			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			mainActivity.setTitle(navMenuTitles[position]);
			}
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position, 0);
		}
	}

	public void removeSoftKeyboard() {
		InputMethodManager inputMethodManager = (InputMethodManager) mainActivity
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(mainActivity
				.getCurrentFocus().getWindowToken(), 0);
	}
	
	public void alertDialog() {
			// set title
			alertDialogBuilder.setTitle(R.string.already_exists);

			// set dialog message
			alertDialogBuilder
				.setMessage(R.string.try_again)
				.setCancelable(false)
				.setPositiveButton(R.string.okay,new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						//displayView(1);
					}
				  });

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();	
	}
}
