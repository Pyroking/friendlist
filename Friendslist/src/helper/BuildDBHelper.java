package helper;

import java.util.HashMap;
import model.Friend;
import adapter.DBAdapter;

public class BuildDBHelper {

	public void initDatabaseMarkup(DBAdapter db, DateFormatHelper dateFormat) {
		Friend friend = new Friend("S�ren Tryde", "s�ren@gmail.com",
				dateFormat.passStringToDate("07-Jun-2013"));
		Friend friend2 = new Friend("S�ren Tryde2", "s�ren@gmail.com2",
				dateFormat.passStringToDate("07-Jun-2013"));
		db.removeAll();
		db.addContact(friend);
		db.addContact(friend2);
	}

	public HashMap<String, String> createContactList(String key, String name) {
		HashMap<String, String> contact = new HashMap<String, String>();
		contact.put(key, name);

		return contact;
	}
}
