package helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import model.Friend;

public class DateFormatHelper {
	SimpleDateFormat formatter = null;
	Friend friend; 
	
	public DateFormatHelper() {
		friend = new Friend();
		formatter = friend.getFormatter();
	}
	
	public String getDateTime(Date date) {
        return formatter.format(date);
	}
	
	public Date passStringToDate(String stringDate) {
		Date date = null;
		try {
			date = formatter.parse(stringDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
}
