package model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

public class Friend {
    String id;
    String name;
    String email;
    Date birthday;
    
	private static final Locale LOCALE = new Locale("da", "DK");
	
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", LOCALE);
	
	SimpleDateFormat formatterWithoutYear = new SimpleDateFormat("dd-MMM", LOCALE);
    
	public static final Comparator<Friend> NAME_COMPARATOR = new Comparator<Friend>() {

		public int compare(final Friend friend1, final Friend friend2) {
			return friend1.getName().compareTo(friend2.getName());
		}
	};

	public static final Comparator<Friend> BIRTHDAY_COMPARATOR = new Comparator<Friend>() {

		public int compare(final Friend friend1, final Friend friend2) {
			return friend1.birthday.compareTo(friend2.birthday);
		}
	};

	public static final Comparator<Friend> BIRTHDAY_COMPARATOR_EXCLUDING_YEAR = new Comparator<Friend>() {

		public int compare(final Friend friend1, final Friend friend2) {
			final int monthDifference = friend1.getBirthdayMonth() - friend2.getBirthdayMonth();
			if (monthDifference != 0) {
				return monthDifference;
			}
			return friend1.getBirthdayDay() - friend2.getBirthdayDay();
		}
	};
	
	public SimpleDateFormat getFormatter() {
		return formatter;
	}
	
	public SimpleDateFormat getFormatterWithoutYear() {
		return formatterWithoutYear;
	}
    
	public Friend() {
	}

	public Friend(String id, String name, String email, Date birthday) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.birthday = birthday;
	}
	
	public Friend(String name, String email, Date birthday) {
		super();
		this.name = name;
		this.email = email;
		this.birthday = birthday;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	public int getBirthdayMonth() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getBirthday());
        return cal.get(Calendar.MONTH);
	}
	
	public int getBirthdayDay() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getBirthday());
        return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Friend other = (Friend) obj;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name + "\n" + formatter.format(birthday.getTime());
	}
	
	public String toStringNameBirthWithYear() {
		return name + "\n" + formatter.format(birthday.getTime());
	}
	
	public String toStringNameBirthWithOutYear() {
		return name + "\n" + formatterWithoutYear.format(birthday.getTime());
	}
	public String toStringName() {
		return name;
	}
}
